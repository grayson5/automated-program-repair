#lang pollen/markup

◊begin[
  (require
    racket/file
    "lib/mathjax.rkt"
    "lib/theorem.rkt"
	"lib/highlightjs.rkt")
  (define $ math/inline)
  (define $$ math/display)
  (define (note . text) `(@ (em "Note") " " ,@text))
]

◊template-data{
 ◊head[
  math/html-head
  (highlightjs/html-head '("python"))
 ]
 ◊title{Optimal Test Order in Mutation Testing}
}

◊theorem['problem #:unnumbered #t]{We have ◊${n_\text{t}} tests and ◊${n_\text{m}} mutants. We want to order the tests to find a mutant which passes all the tests as quickly by choosing the order of tests and mutants to run. When a mutant fails a test, we say this mutant is ◊em{killed}.}

◊theorem['assumption]{The probability of each test passing is independent of the mutant and other factors. Call the probability of the ◊${i}th test passing ◊${p_i}.}

If this assumption is met, changing the order of mutants won't actually improve the time to first plausible mutant.

◊theorem['assumption]{The duration of each test is independent of the mutant and other factors. Call the distribution of the duration of the ◊${i}th test ◊${d_i}.}

◊theorem['claim]{The time it would take to run ◊${j} tests in the default order is ◊$${D_j := d_0 + d_1 + \dotsb + d_j = \sum\limits_{i=0}^{j+1} d_i.} ◊note{I am using the convention where the lower limit limit on summations is inclusive and the upper is exclusive (likewise for products). This simplifies mathematical expressions as ◊${\sum\limits_{i=a}^b + \sum\limits_{i=b}^c = \sum\limits_{i=a}^c.}}}

◊theorem['claim]{The probability exactly ◊${j} tests in the default order will "kill" is ◊$${P_j := p_0 p_1 p_2 \dotsb p_{j-1} (1-p_j) = (1-p_j) \prod_{i=0}^j p_i.}}

◊note{Finally, there is an event not represented by any ◊${P_i}, which is when the mutant passes all tests; this can be handled by appending fictitious test that always fails in no time, ◊${P_{n_\text{t}} = 0, D_{n_\text{t}} = 0}. This way, there is always a "first failing test", but it might be the fictitious one.}

◊theorem['claim]{The expected value of the amount of time it would take to kill a mutant in the default order is ◊$${E := \sum\limits_{j=0}^{n_\text{t}+1} D_j P_j.}}

I will use the notation ◊${D_{j, k \leftrightarrow \ell}, P_{j, k \leftrightarrow \ell}, E_{k \leftrightarrow \ell}} to describe the duration, probability, and expected values in the case where test ◊${k} is swapped with test ◊${\ell} where ◊${0 \leq k < \ell < n_{\text{t}}}. These can be computed "differentially" from ◊${D_j, P_j, E}.

◊theorem['claim #:nickname 'perturbed-duration]{
◊$${D_{j, k \leftrightarrow \ell} = \left\{\begin{array}{rc}
d_k + d_1 + d_2 + \dotsb + d_j                                  & [j < k] \\
d_k + d_1 + d_2 + \dotsb + d_\ell + \dotsb + d_j                & [k \leq j < \ell] \\
d_k + d_1 + d_2 + \dotsb + d_\ell + \dotsb + d_k + \dotsb + d_j & [\ell \leq j] \\
\end{array}\right\}
= \left\{\begin{array}{rc}
D_j & [j < k] \\
D_j + d_\ell - d_k & [k \leq j < \ell] \\
D_j & [\ell \leq j] \\
\end{array}\right\}
.}}

◊theorem['claim #:nickname 'perturbed-probability]{
◊$${P_{j, k \leftrightarrow \ell}
= \left\{\begin{array}{rc}
p_0 p_1 p_2 \dotsb p_{j-1} (1 - p_j) & [j < k] \\
p_0 p_1 p_2 \dotsb p_{k-1} (1-p_\ell) & [j = k] \\
p_0 p_1 p_2 \dotsb p_\ell \dotsb p_{j-1} (1-p_j) & [k < j < \ell] \\
p_0 p_1 p_2 \dotsb p_{\ell-1} (1-p_k) & [j = \ell] \\
p_0 p_1 p_2 \dotsb p_\ell \dotsb p_k \dotsb p_{j-1} (1 - p_j) & [\ell < j] \\
\end{array}\right\}
= \left\{\begin{array}{rc}
P_j & [j < k] \\
\frac{1 - p_\ell}{1 - p_k} P_k & [j = k] \\
\frac{p_\ell}{p_k} P_j & [k < j < \ell] \\
\frac{p_\ell}{p_k} \frac{1 - p_k}{1 - p_\ell} P_\ell & [j = \ell] \\
P_j & [\ell < j] \\
\end{array}\right\}
.}}

◊theorem['claim]{
Using ◊theorem/ref['perturbed-duration] and ◊theorem/ref['perturbed-probability], we have
◊$${
\begin{array}{rl}
E_{0 \leftrightarrow \ell}
& = \sum\limits_{j=0}^{n_\text{t}+1} D_{j, 0 \leftrightarrow \ell} P_{j, 0 \leftrightarrow \ell} \\
& = D_{0, 0 \leftrightarrow \ell} P_{0, 0 \leftrightarrow \ell} + \sum\limits_{j=1}^{\ell} D_{j, 0 \leftrightarrow \ell} P_{j, 0 \leftrightarrow \ell} + D_{\ell, 0 \leftrightarrow \ell} P_{\ell, 0 \leftrightarrow \ell} + \sum\limits_{j=\ell+1}^{n_\text{t} + 1} D_{j, 0 \leftrightarrow \ell} P_{j, 0 \leftrightarrow \ell} \\
& = d_\ell(1-p_\ell) + \frac{p_\ell}{p_0}\sum\limits_{j=1}^{\ell} (D_j - d_0 + d_\ell) P_j + \frac{p_\ell}{p_0} \frac{1 - p_0}{1 - p_\ell} D_\ell P_\ell + \sum\limits_{j=\ell+1}^{n_\text{t}+1} D_j P_j \\
& = d_\ell(1-p_\ell) + \frac{p_\ell}{p_0} \left[\sum\limits_{j=1}^{\ell} D_j P_j\right] + \frac{p_\ell}{p_0}(d_\ell-d_0)\left[\sum\limits_{j=1}^{\ell}P_j\right] + \frac{p_\ell}{p_0} \frac{1 - p_0}{1 - p_\ell} D_\ell P_\ell + \left[\sum\limits_{j=\ell+1}^{n_\text{t}+1} D_j P_j\right]. \\
\end{array}
}}

The quantities in [square brackets] in the last line can be precomputed for all ◊${k} in ◊${\mathcal{O}(n_\text{t})} using dynamic programming. Evaluating one ◊${E_{0 \leftrightarrow \ell}} is ◊${\mathcal{O}(1)}.

◊theorem['algorithm]{◊code{best_order} should compute the best order to the tests, using the assumptions above.

◊highlightjs/code["python"]{
◊file->string["algo.py"]
}
}
