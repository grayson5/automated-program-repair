#lang racket

(require
 racket/match
 racket/string
 rackunit
 txexpr)

(define
  (title-case text)
  (string-join
   (map
	(λ (word)
	  (match
	   (string->list word)
	   [(list first rest ...) (list->string (cons (char-upcase first) rest))]
	   [(list) ""]))
	(string-split text))
   " "))

(check-equal? (title-case "abc def") "Abc Def")

(struct theorem-state (id-map nickname-map))

(define global-theorem-state (theorem-state (make-hash) (make-hash)))

(define
  (theorem-state-next-id this type)
  (let
	  ([id (add1 (hash-ref (theorem-state-id-map this) type 0))])
	(hash-set! (theorem-state-id-map this) type id)
	id))

(define
  (theorem-state-define-nickname this type nickname id)
  (if
   (hash-has-key? (theorem-state-nickname-map this) nickname)
   (error "Duplicate nickname" nickname)
   (hash-set! (theorem-state-nickname-map this) nickname `(,type . ,id))))

(define
  (theorem
   type
   #:nickname [nickname null]
   #:unnumbered [unnumbered #f]
   #:state [this global-theorem-state]
   . text)
  (let*
	  ([id (theorem-state-next-id this type)])
	(if (null? nickname)
		null
		(theorem-state-define-nickname this type nickname id))
	(txexpr
	 'p
	 '()
	 `((strong
		,(title-case (symbol->string type))
		,@(if unnumbered '() `(" " ,(number->string id))))
	   " "
	   ,@text))))

(define
  (theorem/ref
   nickname
   #:state [this global-theorem-state])
  (match
   (hash-ref
	(theorem-state-nickname-map this)
	nickname
	(λ ()
	  (raise-user-error
	   "nickname "
	   nickname
	   " not found in "
	   (hash-keys (theorem-state-nickname-map this)))))
   [(cons type id)
	(txexpr* 'span '() (title-case (symbol->string type)) " " (number->string id))]))

(test-begin
 (let ([test-state (theorem-state (make-hash) (make-hash))])
   (check-pred txexpr? (theorem 'lemma #:state test-state))
   (check-pred txexpr? (theorem 'lemma #:unnumbered #t #:state test-state))))

(test-begin
 (let ([test-state (theorem-state (make-hash) (make-hash))])
   (check-pred txexpr? (theorem 'lemma #:nickname 'foo #:state test-state))
   (check-pred txexpr? (theorem/ref 'foo #:state test-state))
   (check-exn exn:fail:user? (λ () (theorem/ref 'bar #:state test-state)))))

(provide theorem theorem/ref)
