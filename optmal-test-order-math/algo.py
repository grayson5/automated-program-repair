from typing import List
import numpy.typing as npt
import numpy as np

def reverse(array: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
    """Reverse a numpy array"""
    return array[::-1]

def cumsum(
        array: npt.NDArray[np.float64],
        inclusive: bool = True,
        one_larger_output: bool = False,
) -> npt.NDArray[np.float64]:
    if one_larger_output:
        assert not inclusive
    ret = np.zeros(len(array) + int(one_larger_output))
    for i in range(int(not inclusive), len(ret)):
        ret[i] = ret[i-1] + array[i - int(not inclusive)]
    return ret

def cumprod(
        array: npt.NDArray[np.float64],
        inclusive: bool = True,
        one_larger_output: bool = False,
) -> npt.NDArray[np.float64]:
    if one_larger_output:
        assert not inclusive
    ret = np.ones(len(array) + int(one_larger_output))
    for i in range(int(not inclusive), len(ret)):
        ret[i] = ret[i-1] * array[i - int(not inclusive)]
    return ret

def next_best(d: npt.NDArray[np.float64], p: npt.NDArray[np.float64]) -> List[int]:
    """
    Returns the index of the best test to run.
    """

    if len(d) == 1:
        return 0

    assert len(d) == len(p)

    # D[ell] := d[0] + d[1] + ... + d[ell]
    D = cumsum(d)
    # P[ell] := p[0] * p[1] * ... * p[ell-1] * (1 - p[ell])
    P = cumprod(p, inclusive=False) * (1 - p)
    # A[ell] := D[1] * P[1] + D[2] * P[2] + ... + D[ell - 1] * P[ell - 1] for ell > 0
    A = cumsum(D * P, inclusive=False) - D[0] * P[0]
    # B[ell] := P[1] + P[2] + ... + P[ell - 1] for ell > 0
    B = cumsum(P, inclusive=False) - P[0]
    # C[ell] := D[ell+1] * P[ell+1] + D[ell+2] * P[ell+2] + ... + D[len(D) - 1] * P[len(D) - 1]
    # TODO: this
    C = np.cumsum(reverse(D * P))

    # Compute E of the swapped order 0 <-> k for all k.
    # An array, X, with no indexes should be interpreted as "X[ell], for all ell".
    E_of_swap = (
        d*(1-p)
        + p/p[0]*A
        + p/p[0]*(d-d[0])*B
        + p[0]/p*(1-p[0])/(1-p)*D*P
        + C
    )

    return np.argmin(E_of_swap)

def best_order(
        d: npt.NDArray[np.float64],
        p: npt.NDArray[np.float64],
) -> npt.NDArray[int]:
    """
    Returns a list of indices which consist the best order to run.
    """

    assert len(d) == len(p)

    tests = np.arange(len(d), dtype=int)

    # Loop through each test and find the best order.
    for k in range(len(d)):
        # The first k have already been assigned.
        best = next_best(d[k:], p[k:])

        # Swap best with k, so that arr[k+1:] are the remaining tests
        if k != best:
            p[[k, best]] = p[[best, k]]
            d[[k, best]] = d[[best, k]]
            tests[[k, best]] = tests[[best, k]]

    assert list(sorted(tests)) == list(range(len(d)))

    return tests
