{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    raco-pkg-env.url = "github:charmoniumQ/raco-pkg-env";
  };

  outputs = { self, nixpkgs, flake-utils, raco-pkg-env }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            raco-pkg-env.packages.${system}.raco-pkg-env
          ];
          shellHook = ''
            if [ ! -d env ]; then
              raco-pkg-env env
              raco pkg install --auto pollen rackunit
            fi
            . ./env/activate.sh
          '';
        };
      });
}
