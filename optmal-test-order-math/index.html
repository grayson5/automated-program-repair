<!DOCTYPE html>
<html lang="en"><head><meta charset="utf-8"/><title>Optimal Test Order in Mutation Testing</title><script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script><script>Mathjax = {};</script><script id="MathJax-script" async="true" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/default.min.css"/><script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script><script src="https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.3.1/build/languages/python.min.js"></script><script>hljs.highlightAll();</script></head><body><h1>Optimal Test Order in Mutation Testing</h1>

<p><strong>Problem</strong> We have \(n_\text{t}\) tests and \(n_\text{m}\) mutants. We want to order the tests to find a mutant which passes all the tests as quickly by choosing the order of tests and mutants to run. When a mutant fails a test, we say this mutant is <em>killed</em>.</p>

<p><strong>Assumption 1</strong> The probability of each test passing is independent of the mutant and other factors. Call the probability of the \(i\)th test passing \(p_i\).</p>

If this assumption is met, changing the order of mutants won't actually improve the time to first plausible mutant.

<p><strong>Assumption 2</strong> The duration of each test is independent of the mutant and other factors. Call the distribution of the duration of the \(i\)th test \(d_i\).</p>

<p><strong>Claim 1</strong> The time it would take to run \(j\) tests in the default order is <div>\[D_j := d_0 + d_1 + \dotsb + d_j = \sum\limits_{i=0}^{j+1} d_i.\]</div> <em>Note</em> I am using the convention where the lower limit limit on summations is inclusive and the upper is exclusive (likewise for products). This simplifies mathematical expressions as \(\sum\limits_{i=a}^b + \sum\limits_{i=b}^c = \sum\limits_{i=a}^c.\)</p>

<p><strong>Claim 2</strong> The probability exactly \(j\) tests in the default order will "kill" is <div>\[P_j := p_0 p_1 p_2 \dotsb p_{j-1} (1-p_j) = (1-p_j) \prod_{i=0}^j p_i.\]</div></p>

<em>Note</em> Finally, there is an event not represented by any \(P_i\), which is when the mutant passes all tests; this can be handled by appending fictitious test that always fails in no time, \(P_{n_\text{t}} = 0, D_{n_\text{t}} = 0\). This way, there is always a "first failing test", but it might be the fictitious one.

<p><strong>Claim 3</strong> The expected value of the amount of time it would take to kill a mutant in the default order is <div>\[E := \sum\limits_{j=0}^{n_\text{t}+1} D_j P_j.\]</div></p>

I will use the notation \(D_{j, k \leftrightarrow \ell}, P_{j, k \leftrightarrow \ell}, E_{k \leftrightarrow \ell}\) to describe the duration, probability, and expected values in the case where test \(k\) is swapped with test \(\ell\) where \(0 \leq k &lt; \ell &lt; n_{\text{t}}\). These can be computed "differentially" from \(D_j, P_j, E\).

<p><strong>Claim 4</strong> <div>\[D_{j, k \leftrightarrow \ell} = \left\{\begin{array}{rc}
d_k + d_1 + d_2 + \dotsb + d_j                                  &amp; [j &lt; k] \\
d_k + d_1 + d_2 + \dotsb + d_\ell + \dotsb + d_j                &amp; [k \leq j &lt; \ell] \\
d_k + d_1 + d_2 + \dotsb + d_\ell + \dotsb + d_k + \dotsb + d_j &amp; [\ell \leq j] \\
\end{array}\right\}
= \left\{\begin{array}{rc}
D_j &amp; [j &lt; k] \\
D_j + d_\ell - d_k &amp; [k \leq j &lt; \ell] \\
D_j &amp; [\ell \leq j] \\
\end{array}\right\}
.\]</div></p>

<p><strong>Claim 5</strong> <div>\[P_{j, k \leftrightarrow \ell}
= \left\{\begin{array}{rc}
p_0 p_1 p_2 \dotsb p_{j-1} (1 - p_j) &amp; [j &lt; k] \\
p_0 p_1 p_2 \dotsb p_{k-1} (1-p_\ell) &amp; [j = k] \\
p_0 p_1 p_2 \dotsb p_\ell \dotsb p_{j-1} (1-p_j) &amp; [k &lt; j &lt; \ell] \\
p_0 p_1 p_2 \dotsb p_{\ell-1} (1-p_k) &amp; [j = \ell] \\
p_0 p_1 p_2 \dotsb p_\ell \dotsb p_k \dotsb p_{j-1} (1 - p_j) &amp; [\ell &lt; j] \\
\end{array}\right\}
= \left\{\begin{array}{rc}
P_j &amp; [j &lt; k] \\
\frac{1 - p_\ell}{1 - p_k} P_k &amp; [j = k] \\
\frac{p_\ell}{p_k} P_j &amp; [k &lt; j &lt; \ell] \\
\frac{p_\ell}{p_k} \frac{1 - p_k}{1 - p_\ell} P_\ell &amp; [j = \ell] \\
P_j &amp; [\ell &lt; j] \\
\end{array}\right\}
.\]</div></p>

<p><strong>Claim 6</strong> Using <span>Claim 4</span> and <span>Claim 5</span>, we have
<div>\[\begin{array}{rl}
E_{0 \leftrightarrow \ell}
&amp; = \sum\limits_{j=0}^{n_\text{t}+1} D_{j, 0 \leftrightarrow \ell} P_{j, 0 \leftrightarrow \ell} \\
&amp; = D_{0, 0 \leftrightarrow \ell} P_{0, 0 \leftrightarrow \ell} + \sum\limits_{j=1}^{\ell} D_{j, 0 \leftrightarrow \ell} P_{j, 0 \leftrightarrow \ell} + D_{\ell, 0 \leftrightarrow \ell} P_{\ell, 0 \leftrightarrow \ell} + \sum\limits_{j=\ell+1}^{n_\text{t} + 1} D_{j, 0 \leftrightarrow \ell} P_{j, 0 \leftrightarrow \ell} \\
&amp; = d_\ell(1-p_\ell) + \frac{p_\ell}{p_0}\sum\limits_{j=1}^{\ell} (D_j - d_0 + d_\ell) P_j + \frac{p_\ell}{p_0} \frac{1 - p_0}{1 - p_\ell} D_\ell P_\ell + \sum\limits_{j=\ell+1}^{n_\text{t}+1} D_j P_j \\
&amp; = d_\ell(1-p_\ell) + \frac{p_\ell}{p_0} \left[\sum\limits_{j=1}^{\ell} D_j P_j\right] + \frac{p_\ell}{p_0}(d_\ell-d_0)\left[\sum\limits_{j=1}^{\ell}P_j\right] + \frac{p_\ell}{p_0} \frac{1 - p_0}{1 - p_\ell} D_\ell P_\ell + \left[\sum\limits_{j=\ell+1}^{n_\text{t}+1} D_j P_j\right]. \\
\end{array}\]</div></p>

The quantities in [square brackets] in the last line can be precomputed for all \(k\) in \(\mathcal{O}(n_\text{t})\) using dynamic programming. Evaluating one \(E_{0 \leftrightarrow \ell}\) is \(\mathcal{O}(1)\).

<p><strong>Algorithm 1</strong> <code>best_order</code> should compute the best order to the tests, using the assumptions above.

<div><pre><code class="language-python">from typing import List
import numpy.typing as npt
import numpy as np

def reverse(array: npt.NDArray[np.float64]) -&gt; npt.NDArray[np.float64]:
    """Reverse a numpy array"""
    return array[::-1]

def cumsum(
        array: npt.NDArray[np.float64],
        inclusive: bool = True,
        one_larger_output: bool = False,
) -&gt; npt.NDArray[np.float64]:
    if one_larger_output:
        assert not inclusive
    ret = np.zeros(len(array) + int(one_larger_output))
    for i in range(int(not inclusive), len(ret)):
        ret[i] = ret[i-1] + array[i - int(not inclusive)]
    return ret

def cumprod(
        array: npt.NDArray[np.float64],
        inclusive: bool = True,
        one_larger_output: bool = False,
) -&gt; npt.NDArray[np.float64]:
    if one_larger_output:
        assert not inclusive
    ret = np.ones(len(array) + int(one_larger_output))
    for i in range(int(not inclusive), len(ret)):
        ret[i] = ret[i-1] * array[i - int(not inclusive)]
    return ret

def next_best(d: npt.NDArray[np.float64], p: npt.NDArray[np.float64]) -&gt; List[int]:
    """
    Returns the index of the best test to run.
    """

    if len(d) == 1:
        return 0

    assert len(d) == len(p)

    # D[ell] := d[0] + d[1] + ... + d[ell]
    D = cumsum(d)
    # P[ell] := p[0] * p[1] * ... * p[ell-1] * (1 - p[ell])
    P = cumprod(p, inclusive=False) * (1 - p)
    # A[ell] := D[1] * P[1] + D[2] * P[2] + ... + D[ell - 1] * P[ell - 1] for ell &gt; 0
    A = cumsum(D * P, inclusive=False) - D[0] * P[0]
    # B[ell] := P[1] + P[2] + ... + P[ell - 1] for ell &gt; 0
    B = cumsum(P, inclusive=False) - P[0]
    # C[ell] := D[ell+1] * P[ell+1] + D[ell+2] * P[ell+2] + ... + D[len(D) - 1] * P[len(D) - 1]
    # TODO: this
    C = np.cumsum(reverse(D * P))

    # Compute E of the swapped order 0 &lt;-&gt; k for all k.
    # An array, X, with no indexes should be interpreted as "X[ell], for all ell".
    E_of_swap = (
        d*(1-p)
        + p/p[0]*A
        + p/p[0]*(d-d[0])*B
        + p[0]/p*(1-p[0])/(1-p)*D*P
        + C
    )

    return np.argmin(E_of_swap)

def best_order(
        d: npt.NDArray[np.float64],
        p: npt.NDArray[np.float64],
) -&gt; npt.NDArray[int]:
    """
    Returns a list of indices which consist the best order to run.
    """

    assert len(d) == len(p)

    tests = np.arange(len(d), dtype=int)

    # Loop through each test and find the best order.
    for k in range(len(d)):
        # The first k have already been assigned.
        best = next_best(d[k:], p[k:])

        # Swap best with k, so that arr[k+1:] are the remaining tests
        if k != best:
            p[[k, best]] = p[[best, k]]
            d[[k, best]] = d[[best, k]]
            tests[[k, best]] = tests[[best, k]]

    assert list(sorted(tests)) == list(range(len(d)))

    return tests
</code></pre></div></p>
</body></html>