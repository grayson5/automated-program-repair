import abc
from typing import Tuple
import numpy.typing as npt
import numpy as np
import scipy.stats
from algo import best_order

class Strategy(abc.ABC):
    @abc.abstractmethod
    def feed_result(self, mutant: int, test: int, time: np.float64, is_pass: bool) -> None:
        ...

    @abc.abstractmethod
    def get_next(self) -> Tuple[int, int]:
        ...

class MutantAtATimeStrategy(Strategy):
    def __init__(self, n_tests: int, n_mutants: int) -> None:
        self.n_tests = n_tests
        self.n_mutants = n_mutants
        self.empirical_time_matrix = np.zeros(n_tests, dtype=np.float64)
        self.empirical_pass_matrix = np.zeros(n_tests, dtype=int)
        self.empirical_tested_matrix = np.zeros(n_tests, dtype=int)
        self.current_mutant = 0
        self.current_test = 0
        self.order = self.default_order()

    def feed_result(self, mutant: int, test: int, time: np.float64, is_pass: bool) -> None:
        self.empirical_time_matrix[test] += time
        self.empirical_pass_matrix[test] += int(is_pass)
        self.empirical_tested_matrix[test] += 1

    def get_next(self) -> Tuple[int, int]:
        if self.current_test == self.n_tests:
            self.order = self.compute_order()
            self.current_test = 0
            self.current_mutant += 1
        ret = (self.current_mutant, self.order[self.current_test])
        self.current_test += 1
        return ret

    def default_order(self) -> None:
        return list(range(self.n_tests))

    @abc.abstractmethod
    def compute_order(self) -> None:
        ...

class PassRateStrategy(MutantAtATimeStrategy):
    def compute_order(self) -> None:
        empirical_avg_pass_rate = self.empirical_pass_matrix / self.empirical_tested_matrix
        return np.argsort(empirical_avg_pass_rate)


class OptimalStrategy(MutantAtATimeStrategy):
    def compute_order(self) -> None:
        d = self.empirical_time_matrix / self.empirical_tested_matrix
        p = self.empirical_pass_matrix / self.empirical_tested_matrix
        d[np.isnan(d)] = np.average(d[~np.isnan(d)])
        p[np.isnan(p)] = np.average(p[~np.isnan(p)])
        return best_order(d, p)

def random_matrix(
        n_tests: int,
        n_mutants: int,
        feasible_rate: float,
        left_exponent: float,
) -> Tuple[npt.NDArray[np.float64], npt.NDArray[bool]]:
    test_duration = np.random.rand(n_tests) * 60
    time_matrix = np.tile(test_duration[:, np.newaxis], (n_tests, n_mutants))
    pass_matrix = np.ones((n_tests, n_mutants), dtype=bool)
    for mutant in range(n_mutants):
        if np.random.rand(1)[0] > feasible_rate:
            p = 1
            for test in range(n_tests):
                p *= left_exponent
                if np.random.rand(1)[0] < p:
                    pass_matrix[mutant, test] = False
                    break
            else:
                pass_matrix[mutant, 0] = False
    return time_matrix, pass_matrix

def simulate(
        time_matrix: npt.NDArray[np.float64],
        pass_matrix: npt.NDArray[bool],
        strategy: Strategy,
) -> np.float64:
    total_time = 0
    observed_pass_matrix = np.zeros_like(pass_matrix, dtype=int)
    # observed_pass_matrix[mutant, test] == 1 implies mutant/test failed
    # observed_pass_matrix[mutant, test] == 2 implies mutant/test passed
    # observed_pass_matrix[mutant, test] == 0 implies mutant/test is unknown
    while True:
        mutant, test = strategy.get_next()
        strategy.feed_result(mutant, test, time_matrix[mutant, test], pass_matrix[mutant, test])
        total_time += time_matrix[mutant, test]
        observed_pass_matrix[mutant, test] = pass_matrix[mutant, test] + 1
        if (observed_pass_matrix[:, mutant] == 2).all():
            found = True
            break
        if (observed_pass_matrix != 0).all():
            found = False
            break
    return total_time, observed_pass_matrix

def main_simulation(
        n_iterations: int = 30,
        n_tests: int = 10,
        n_mutants: int = 10,
        confidence_level: float = .95,
        feasible_rate: float = 0.9,
        left_exponent: float = 0.5,
):
    naive_times = []
    optimal_times = []
    for _ in range(n_iterations):
        time_matrix, pass_matrix = random_matrix(n_tests, n_mutants, feasible_rate, left_exponent)
        naive_time, _ = simulate(time_matrix, pass_matrix, PassRateStrategy(n_tests, n_mutants))
        naive_times.append(naive_time)
        optimal_time, _ = simulate(time_matrix, pass_matrix, OptimalStrategy(n_tests, n_mutants))
        optimal_times.append(optimal_time)
    naive_times = np.array(naive_times)
    optimal_times = np.array(optimal_times)
    res = scipy.stats.bootstrap(
        data=((naive_times - optimal_times) / naive_times,),
        statistic=scipy.stats.gmean,
        confidence_level=confidence_level,
        method="percentile",
    )
    print(f"geomean of speedup of optimal algorithm over naive is {scipy.stats.gmean((naive_times - optimal_times) / naive_times)*100:.0f}% of naive speed.")
    print(f"{confidence_level*100}% confident that speedup of the optimal algorithm lies between {res.confidence_interval.low*100:.0f}% and {res.confidence_interval.high*100:.0f}% of naive speed.")

if __name__ == "__main__":
    main_simulation()
